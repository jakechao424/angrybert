validation folder 1
	eval task founta precision: 80.26 
	eval task founta recall: 80.94 
	eval task founta f1: 80.51 
	eval task founta hate precision: 47.00 
	eval task founta hate recall: 44.00 
	eval task founta hate f1: 45.00 

	eval task hatelingo precision: 93.13 
	eval task hatelingo recall: 92.95 
	eval task hatelingo f1: 92.50 

	eval task semeval_c precision: 69.37 
	eval task semeval_c recall: 70.99 
	eval task semeval_c f1: 69.13 

	eval task emotion roc auc score for multi label classification: 86.96 

validation folder 2
	eval task founta precision: 80.89 
	eval task founta recall: 81.76 
	eval task founta f1: 81.19 
	eval task founta hate precision: 54.00 
	eval task founta hate recall: 39.00 
	eval task founta hate f1: 45.00 

	eval task hatelingo precision: 91.35 
	eval task hatelingo recall: 93.04 
	eval task hatelingo f1: 92.08 

	eval task semeval_c precision: 66.99 
	eval task semeval_c recall: 70.99 
	eval task semeval_c f1: 67.83 

	eval task emotion roc auc score for multi label classification: 87.31 

validation folder 3
	eval task founta precision: 80.63 
	eval task founta recall: 81.58 
	eval task founta f1: 80.94 
	eval task founta hate precision: 52.00 
	eval task founta hate recall: 39.00 
	eval task founta hate f1: 44.00 

	eval task hatelingo precision: 99.21 
	eval task hatelingo recall: 99.21 
	eval task hatelingo f1: 99.21 

	eval task semeval_c precision: 68.34 
	eval task semeval_c recall: 70.87 
	eval task semeval_c f1: 68.86 

	eval task emotion roc auc score for multi label classification: 86.56 

validation folder 4
	eval task founta precision: 80.99 
	eval task founta recall: 81.35 
	eval task founta f1: 81.12 
	eval task founta hate precision: 55.00 
	eval task founta hate recall: 45.00 
	eval task founta hate f1: 50.00 

	eval task hatelingo precision: 99.12 
	eval task hatelingo recall: 99.12 
	eval task hatelingo f1: 99.12 

	eval task semeval_c precision: 68.74 
	eval task semeval_c recall: 71.85 
	eval task semeval_c f1: 69.16 

	eval task emotion roc auc score for multi label classification: 87.70 

