import errno
import os
import json
import numpy as np
import torch
from captum.attr import visualization as viz
from captum.attr import IntegratedGradients, LayerIntegratedGradients, configure_interpretable_embedding_layer, InterpretableEmbeddingBase, remove_interpretable_embedding_layer
import pickle as pkl
from transformers import BertTokenizer
import matplotlib.pyplot as plt
import itertools

from sklearn.metrics import confusion_matrix

def assert_exits(path):
    assert os.path.exists(path), 'Does not exist : {}'.format(path)
    
def equal_info(a,b):
    assert len(a)==len(b),'File info not equal!'
    
def same_question(a,b):
    assert a==b,'Not the same question!'
    
class Logger(object):
	def __init__(self,output_dir):
		dirname=os.path.dirname(output_dir)
		if not os.path.exists(dirname):
			os.mkdir(dirname)
		self.log_file=open(output_dir,'w')
		self.infos={}
		
	def append(self,key,val):
		vals=self.infos.setdefault(key,[])
		vals.append(val)

	def log(self,extra_msg=''):
		msgs=[extra_msg]
		for key, vals in self.infos.iteritems():
			msgs.append('%s %.6f' %(key,np.mean(vals)))
		msg='\n'.joint(msgs)
		self.log_file.write(msg+'\n')
		self.log_file.flush()
		self.infos={}
		return msg
		
	def write(self,msg):
		self.log_file.write(msg+'\n')
		self.log_file.flush()
		print(msg)

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def attention_dump(opt,model,test_set, idx):
    # print ("The information for task {} # iterations : {} last batch : {}".format(source, len(test_set),test_set.last_batch))
    tasks=opt.TASKS.split(',')
    total = len(test_set)
    words_list = []

    weights_info = {"0":[],"1":[],"2":[],"3":[]}
    preds_info = {"0":[],"1":[],"2":[],"3":[]}
    
    for i in range(total):
        with torch.no_grad():
            batch_info=test_set.next_batch()
            tokens=batch_info['tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            bert_tokens=batch_info['bert_tokens'].cuda()
            masks=batch_info['masks'].cuda()
            att_masks = batch_info['att_masks'].cuda()
            words=batch_info['words']
            words_list.extend(words)
            for task_idx in range(len(tasks)):
                pred,weights=model(tokens,task_idx,bert_tokens,masks,att_masks)
                weights_info[str(task_idx)].extend(weights.cpu().numpy())
                preds_info[str(task_idx)].extend(pred.cpu().numpy())
        if i==0:
            #print ('yes')
            t1_labels=labels
        else:
            t1_labels=torch.cat((t1_labels,labels),0)
    t1_labels = t1_labels.cpu().numpy()
    write_json(words_list, weights_info, preds_info, t1_labels, tasks[0],idx)


def write_json(sent_tokens, weights, preds, labels, source,cv_idx):
    items = []
    for idx in range(len(sent_tokens)):
        weights_tuple = (weights["0"][idx],weights["1"][idx],weights["2"][idx],weights["3"][idx])
        preds_tuple = (preds["0"][idx],preds["1"][idx],preds["2"][idx],preds["3"][idx])
        item = {"words": sent_tokens[idx], "weights": weights_tuple, "predictions": preds_tuple, "label": np.argmax(labels[idx])}
        items.append(item)

    with open("weights/{}.attentions.json".format(source+str(cv_idx)), "w") as f:
        json.dump(items, f, cls=NpEncoder)

def get_save_dir(base_dir, name, id_max=100):
    for uid in range(1, id_max):
        save_dir = os.path.join(base_dir, f'{name}-{uid:02d}')
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
            return save_dir

def visualize(tbx, preds, labels, epoch, num_visuals):
    """Visualize text examples to TensorBoard.

    Args:
        tbx (tensorboardX.SummaryWriter): Summary writer.
        pred_dict (dict): dict of predictions of the form id -> pred.
        step (int): Number of examples seen so far during training.
        split (str): Name of data split being visualized.
        num_visuals (int): Number of visuals to select at random from preds.
    """
    if num_visuals <= 0:
        return
    if num_visuals > preds.shape[0]:
        num_visuals = preds.shape[0]
    id2index = {curr_id : idx for idx, curr_id in enumerate(gold_dict['id'])}
    visual_ids = [i for i in range(num_visuals)]
    for i, id_ in enumerate(visual_ids):
        pred = preds[id_]
        gold = labels[id_]
        tbl_fmt = (f'- **Question:** {question}\n'
                   + f'- **Context:** {context}\n'
                   + f'- **Answer:** {gold}\n'
                   + f'- **Prediction:** {pred}')
        tbx.add_text(tag=f'{split}/{i+1}_of_{num_visuals}',
                     text_string=tbl_fmt,
                     global_step=step)

def load_pickle(path):
    with open(path, 'rb') as f:
        obj = pkl.load(f)
    return obj

def clean_sents(sents, add_tokens=False):
    new_sents = []
    for sent in sents:
        new_sent = []
        for word in sent:
            if word != "PAD":
                new_sent.append(word)
        if add_tokens:
            new_sent.append("[SEP]")
            new_sent.insert(0, "[CLS]")
        new_sents.append(new_sent)
    return new_sents

def find_feature_attributions(opt,model,test_set,task_idx=0):
    data = []
    task=opt.TASKS.split(',')[task_idx]
    total = len(test_set)

    model.train()

    batch_info=test_set.next_batch()
    tokens=batch_info['tokens'].cuda()
    sents = clean_sents(batch_info['words'], False)
    bert_tokens=batch_info['bert_tokens'].cuda()
    labels=batch_info['label'].float().cuda()
    masks=batch_info['masks'].cuda()
    att_masks=batch_info['att_masks'].cuda()
    for i in range(tokens.shape[0]):
        curr_tokens = tokens[i].view(1,-1)
        curr_bert_tokens = bert_tokens[i].view(1,-1)
        curr_label = labels[i].view(1,-1)
        curr_mask = masks[i].view(1,-1)
        curr_att_mask = att_masks[i].view(1,-1)
        sent = sents[i]
        score_vis = do_feature_attribution(model, curr_tokens, curr_bert_tokens, curr_label, curr_mask, curr_att_mask, sent)
        data.append(score_vis)
    viz.visualize_text(data)
    model.train(False)

def do_feature_attribution(model, tokens, bert_tokens, label, mask, att_mask, sent, task_idx=0):

    def angrybert_forward(tokens, bert_tokens, task_idx, masks, att_masks):
        return model(tokens, task_idx, bert_tokens, masks, att_masks)[0]

    tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')
    pad_id = tokenizer.pad_token_id
    cls_id = tokenizer.cls_token_id
    sep_id = tokenizer.sep_token_id
    embedding_list = torch.nn.ModuleList()
    # embedding_list.append(model.w_emb)
    #embedding_list.append()
    ig = IntegratedGradients(angrybert_forward)

    # Create baselines
    baseline_tokens = torch.zeros((1, tokens.shape[1]), dtype=torch.long).cuda()
    baseline_bert = [pad_id] * (bert_tokens.shape[1] - 2)
    baseline_bert = [cls_id] + baseline_bert + [sep_id]
    baseline_bert = torch.cuda.LongTensor([baseline_bert]).view(1,-1)

    bert_emb = model.s_rnn.bert.embeddings
    intepretable_shared_emb = configure_interpretable_embedding_layer(model, 'w_emb')
    temp = model.embs[task_idx]
    intepretable_private_emb = InterpretableEmbeddingBase(model.embs[task_idx], 'private_emb')
    model.embs[task_idx] = intepretable_private_emb
    # tokens_emb = intepretable_shared_emb(tokens)
    # intepretable_private_emb = intepretable_private_emb(tokens)
    bert_token_embeddings =  bert_emb(bert_tokens)
    bert_ref_embeddings = bert_emb(baseline_bert)
    token_embeddings = intepretable_private_emb.indices_to_embeddings(tokens)

    inputs = (token_embeddings, bert_token_embeddings)
    baselines = (intepretable_private_emb.indices_to_embeddings(baseline_tokens), bert_ref_embeddings)

    
    with torch.no_grad():
        pred_scores,_ = model(token_embeddings, task_idx, bert_token_embeddings, mask, att_mask)

        pred_class=np.argmax(pred_scores.cpu().numpy(),axis=1)

    true_class = np.argmax(label.cpu().numpy())
    attributions, delta = ig.attribute(
        inputs,
        baselines,
        target=torch.tensor([pred_class]),
        return_convergence_delta=True,
        additional_forward_args = (task_idx, mask, att_mask))
    
    curr_attributions = attributions[0]
    
    attribution_scores = curr_attributions.sum(dim=2).squeeze(0)
    attribution_scores = attribution_scores / torch.norm(attribution_scores)
    attribution_scores = attribution_scores.cpu().detach().numpy()

    model.embs[task_idx] = temp
    remove_interpretable_embedding_layer(model, intepretable_shared_emb)


    score_vis = viz.VisualizationDataRecord(
        word_attributions=attribution_scores,
        pred_prob=pred_scores.max(),
        pred_class=pred_class,
        true_class=true_class,
        attr_class=None,
        attr_score=curr_attributions.sum(),
        raw_input=sent,
        convergence_score=delta)



    return score_vis


def get_confusion_matrix(opt,model,test_set,task_idx):
    # print ('The information for task 1 iterations is:',len(test_set),test_set.last_batch)
    task=opt.TASKS.split(',')[task_idx]
    total = len(test_set)
    class_labels = ['racism', 'sexism', 'neither']
    for i in range(total):
        with torch.no_grad():
            batch_info=test_set.next_batch()
            tokens=batch_info['tokens'].cuda()
            bert_tokens=batch_info['bert_tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            masks=batch_info['masks'].cuda()
            att_masks=batch_info['att_masks'].cuda()
            pred,_=model(tokens,task_idx,bert_tokens,masks,att_masks)

            preds=np.argmax(pred.cpu().numpy(),axis=1)
            label=np.argmax(labels.cpu().numpy(),axis=1)
        if i==0:
            gold_labels=label
            predlist=preds
        else:
            predlist=np.concatenate([predlist,preds])
            gold_labels=np.concatenate([gold_labels,label])
    conf_mat=confusion_matrix(gold_labels, predlist)
    plt.figure(figsize=(10,10))
    plot_confusion_matrix(conf_mat, class_labels)

    return total

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
