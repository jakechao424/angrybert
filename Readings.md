**Surveys**
1.  [A Survey on Hate Speech Detection using Natural Language Processing](https://www.aclweb.org/anthology/W17-1101/) (ACL Workshop'17)
2.  [A Survey on Automatic Detection of Hate Speech in Text](https://www.researchgate.net/publication/326725221_A_Survey_on_Automatic_Detection_of_Hate_Speech_in_Text) (CSUR'18)

**Feature Engineering Approach**
1.  [Learning from bullying traces in social media](https://www.aclweb.org/anthology/N12-1084) (NAACL'12)
2.  [Detecting offensive language in social media to protect adolescent online safety](http://www.cse.psu.edu/~sxz16/papers/SocialCom2012.pdf) (PASSAT'12)
3.  [Detecting offensive tweets via topical feature discovery over a large scale twitter corpus](http://www.cs.cmu.edu/~binfan/papers/cikm12_twitter.pdf) (CIKM'12)
4.  [A lexicon-based approach for hate speech detection](https://preventviolentextremism.info/sites/default/files/A%20Lexicon-Based%20Approach%20for%20Hate%20Speech%20Detection.pdf) (IJMUE'15)
5.  [Do characters abuse more than words?](https://www.aclweb.org/anthology/W16-3638) (ACL'16)
6.  [Abusive language detection in online user content](http://yichang-cs.com/yahoo/WWW16_Abusivedetection.pdf) (WWW'16)
7.  [Hateful symbols or hateful people? predictive features for hate speech detection on twitter](https://www.aclweb.org/anthology/N16-2013) (NAACL'16)
8.  [Are You a Racist or Am I Seeing Things? Annotator Influence on Hate Speech Detection on Twitter](https://www.aclweb.org/anthology/W16-5618) (EMNLP'16)
9.  [Content-driven detection of cyberbullying on the instagram social network](https://pdfs.semanticscholar.org/e33b/f3987bbe7a1e45f24704c9e8fce55a18f979.pdf) (IJCAI'16)
10. [Automated Hate Speech Detection and the Problem of Offensive Language](https://www.aaai.org/ocs/index.php/ICWSM/ICWSM17/paper/download/15665/14843) (ICWSM'17)

**Deep Learning Approach**
1.  [Hate Speech Detection with Comment Embeddings](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.697.9571&rep=rep1&type=pdf) (WWW'15)
3.  [Using convolutional neural networks to classify hate-speech](https://www.aclweb.org/anthology/W17-3013) (ACL'17)
4.  [Deep Learning for Hate Speech Detection in Tweets](https://arxiv.org/pdf/1706.00188.pdf) (WWW'17)
2.  [Hate Speech Detection Using a Convolution-LSTM Based Deep Neural Network](http://irep.ntu.ac.uk/id/eprint/34022/1/11440_Tepper.pdf) (ESWC'18)
2.  [Neural Based Statement Classification for Biased Language](https://arxiv.org/pdf/1811.05740.pdf) (WSDM'19)
6.  [Hate Speech Detection is Not as Easy as You May Think: A Closer Look at Model Validation](https://users.dcc.uchile.cl/~jperez/papers/sigir2019.pdf) (SIGIR'19)
7.  [The Use of Deep Learning Distributed Representations in the Identification of Abusive Text](https://www.aaai.org/ojs/index.php/ICWSM/article/download/3215/3083/) (ICWSM'19)

**Other Data Sources**
1.  [Twitter Hate Speech Annodated Data](https://github.com/zeerakw/hatespeech)
2.  [Hate speech dataset from a white supremacist forum](https://github.com/aitor-garcia-p/hate-speech-dataset)
3.  [Kaggle Hate Speech Dataset](https://www.kaggle.com/vkrahul/hate-speech-analysis/data)
4.  [Hate Speech Identification Dataset in Crowdflower](https://data.world/crowdflower/hate-speech-identification) (ICWSM'11)
5.  [Thomas Davidson Hate Speech Dataset](https://github.com/t-davidson/hate-speech-and-offensive-language) (ICWSM'17)
6.  [Hatebase](https://hatebase.org/)
7.  [A Benchmark Dataset for Learning to Intervene in Online Hate Speech](https://arxiv.org/pdf/1909.04251.pdf) (EMNLP'19)
  
**Multi-Task Learning**
1.  [Recurrent neural network for text classification with multi-task learning](https://arxiv.org/pdf/1605.05101.pdf) (IJCAI'16)
2.  [An Overview of Multi-Task Learning in Deep Neural Networks](https://arxiv.org/pdf/1706.05098.pdf) (Arxiv'17)
3.  [Adversarial Multi-task Learning for Text Classification](https://www.aclweb.org/anthology/P17-1001.pdf)(ACL'17)
4.  [Gated Multi-Task Network for Text Classification](https://www.aclweb.org/anthology/N18-2114.pdf)(NAACL-HLT 2018)
5.  [Learning What to Share: Leaky Multi-Task Networkfor Text Classification](https://www.aclweb.org/anthology/C18-1175.pdf)(ACL'18)

**Other Interesting Papers**
1.  [Learning Sentence Embeddings with Auxiliary Tasks for Cross-Domain Sentiment Classification](https://www.aclweb.org/anthology/D16-1023.pdf) (ACL'16)

**Text similarity and Noise Removal**
1. [Enhancing Data Analysis with Noise Removal](https://ieeexplore.ieee.org/document/1583581) (IEEE 2006)
2. [Identifying Mislabeled Training Data](https://arxiv.org/pdf/1106.0219.pdf) (Jair 1999)

