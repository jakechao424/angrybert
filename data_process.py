import pandas
from csv import reader
import pandas as pd
import spacy
import nltk 
from nltk.corpus import wordnet
from nltk.corpus import stopwords
import random

# AngryBert Hate Speech dataset
def build_rnn_dataset(dataframes, tokenizer=lambda s: s.split()):
    if isinstance(dataframes, (list, tuple)):
        df = pd.concat(dataframes)
    else:
        df = dataframes
    X = list(df.loc[:, "tweet"].apply(tokenizer))
    y = list(df.loc[:,"class"])
    return X, y

#df = pandas.read_csv("labeled_data.csv")
#X, y = build_rnn_dataset(df)
#print(X[0])
#print(y[0])

#SelfMA dataset
df = pandas.read_csv("SelfMA Annotations.csv")
df = df.dropna(subset = ["attributive"])
quote_only_df = df.dropna(subset = ["Quote"])
count = quote_only_df['Post ID'].isna().sum()
col_names = list(quote_only_df.columns)
col_names.remove("Text")
col_names.remove("Post ID")
col_names.remove("Quote")
for col in col_names:
    quote_only_df[col].fillna(0, inplace=True)

def synonym_replacement(sentence):
    percent_word_change = 0.3
    random_synonym = None
    modified_sentence = sentence
    sentence_nlp = nlp(sentence)
    ents = [e.text for e in sentence_nlp.ents]
    text_tokens = [word for word in sentence_nlp]
    tokens_without_sw = [word for word in text_tokens if word.text.lower() not in all_stopwords]
    tokens_without_sw_alpha = [word for word in tokens_without_sw if word.is_alpha]
    tokens_without_sw_alpha_names = [word for word in tokens_without_sw_alpha if word.text not in ents]
    tokens = [word for word in text_tokens if word.is_alpha]
    num_words = len(tokens)
    num_words_change = num_words * percent_word_change
    if num_words_change < 0.20:
        num_words_change = 0
    elif num_words_change < 1:
        num_words_change = 1
    else:
        num_words_change = int(num_words_change)
    if len(tokens_without_sw_alpha_names) <= num_words_change:
        num_words_change = len(tokens_without_sw_alpha_names)
    for __ in range(num_words_change):
        curr_replace_bool = False
        while curr_replace_bool is False:
            if tokens_without_sw_alpha_names is None or len(tokens_without_sw_alpha_names) - 1 < 0:
                modified_sentence = sentence
                break
            elif len(tokens_without_sw_alpha_names) - 1 == 0:
                random_token_idx = 0
            else:
                random_token_idx = random.randint(0, len(tokens_without_sw_alpha_names) - 1)
            random_word = tokens_without_sw_alpha_names[random_token_idx]
            synonyms = []
            for syn in wordnet.synsets(random_word.text):
                for l in syn.lemmas():
                    synonyms.append(l.name())
            syn_set = set(synonyms)
            syn_set.discard(random_word.text)
            if len(syn_set) > 0:
                random_synonym = syn_set.pop()
                while random_synonym.lower() == random_word.text.lower() and len(syn_set) > 0:
                    random_synonym = syn_set.pop()
                if random_synonym.lower() != random_word.text.lower():
                    tokens_without_sw_alpha_names = tokens_without_sw_alpha_names.remove(random_word)
                    curr_replace_bool = True
            if curr_replace_bool is False:
                tokens_without_sw_alpha_names.remove(random_word)
            if tokens_without_sw_alpha_names is None or len(tokens_without_sw_alpha_names) == 0:
                modified_sentence = sentence
                break
        if random_synonym is None:
            return modified_sentence
        random_synonym_ls = list(random_synonym)
        if random_word.text[0].isupper():
            random_synonym_ls[0] = random_synonym_ls[0].upper()
        random_synonym = "".join(random_synonym_ls)
        if random_word.text.isupper():
            random_synonym = random_synonym.upper()
        random_synonym = random_synonym.replace("_", " ")
        modified_sentence = modified_sentence.replace(random_word.text, random_synonym)
    return modified_sentence

def data_augmentation(train):
    num_augment_examples = 16
    augmented_train = train.copy()
    new_rows = []
    for _, row in train.iterrows():
        sent = row['Quote']
        aug_sentences = set()
        for i in range(num_augment_examples):
            row_copy = row.copy()
            row_copy['Quote'] = synonym_replacement(sent)
            row_copy['Post ID'] += (i * train.shape[0])

            print(row_copy["Quote"])
            if row_copy['Quote'] not in aug_sentences and row_copy['Quote'] != sent:
                new_rows.append(row_copy)
                aug_sentences.add(row_copy['Quote'])     
    return pd.concat([train] + [pd.DataFrame(new_rows, columns=row_copy.index)], ignore_index=True)

nlp = spacy.load('en_core_web_sm')
nltk.download('stopwords')
all_stopwords = stopwords.words('english')
train_split_df = quote_only_df.sample(frac = 0.8)
test_split_df = quote_only_df.drop(train_split_df.index)
train_split_aug_df = data_augmentation(train_split_df)
train_split_aug_df.to_csv("dataset/train_aug_Self_MA.csv")
test_split_df.to_csv("dataset/test_Self_MA.csv")
#del new_df["Text"]