## Dependencies:  
- Python 3.6
- numpy 1.19.0
- pandas 1.0.3
- nltk
- scikit-learn
- Pytorch 1.5.1


#### Dataset Summary

| Dataset           | #tweets | classes |
|:------------------|:----:|:---:|
|DT                 | 24K     | hateful(1,430), offensive(19,190), neither(4,163)     |
|WZ                 | 16K     | sexism(3,383), racism(1,972), neither(11,559)     |
|Founta             | 99K     | hateful(3,907), offensive(19,232), neither(53,011), spam(13,840)     |
|HateLingo          | 9K      | gender, religion, ethnicity, sexual orientation     |
|SemEval_A          | 9K      | anger, anticipation, disgust, fear, joy, love, optimism, pessimism, sadness, surprise     |
|OffensEval_C       | 4K      | individual(2,507), group(1,152), other(430)   |


You can find processed datasets on `resource` dir

## Getting Started
- You can find all the codes and scripts in `BERT-MTL` folder.
- Modify config.py for required setup.
- Our implementation of AngryBERT can be found on `baseline.py` file. 
- Default model config is set to `angrybert` but you can also find other baselines. Check details on `config.py`.
- Train and evaluate the model, use code in `BERT-MTL`:  

    ``` python main.py ``` 



To cite:
```
@InProceedings{10.1007/978-3-030-75762-5_55,
author="Awal, Md Rabiul
and Cao, Rui
and Lee, Roy Ka-Wei
and Mitrovi{\'{c}}, Sandra",
editor="Karlapalem, Kamal
and Cheng, Hong
and Ramakrishnan, Naren
and Agrawal, R. K.
and Reddy, P. Krishna
and Srivastava, Jaideep
and Chakraborty, Tanmoy",
title="AngryBERT: Joint Learning Target and Emotion for Hate Speech Detection",
booktitle="Advances in Knowledge Discovery and Data Mining",
year="2021",
publisher="Springer International Publishing",
address="Cham",
pages="701--713",
abstract="Automated hate speech detection in social media is a challenging task that has recently gained significant traction in the data mining and Natural Language Processing community. However, most of the existing methods adopt a supervised approach that depended heavily on the annotated hate speech datasets, which are imbalanced and often lack training samples for hateful content. This paper addresses the research gaps by proposing a novel multitask learning-based model, AngryBERT, which jointly learns hate speech detection with sentiment classification and target identification as secondary relevant tasks. We conduct extensive experiments to augment three commonly-used hate speech detection datasets. Our experiment results show that AngryBERT outperforms state-of-the-art single-task-learning and multitask learning baselines. We conduct ablation studies and case studies to empirically examine the strengths and characteristics of our AngryBERT model and show that the secondary tasks are able to improve hate speech detection.",
isbn="978-3-030-75762-5"
}
```

### Correspondence 
Rabiul AWAl (mda219@usask.ca)
